#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

printf "%s\n""\e[0;32m" && echo  "Starting script, please be patient." && printf "\e[0m\n"

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud-files/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

  # download_app_script name_of_script
  download_app_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${APP}/${1}.sh" -P "$SCRIPTS" || wget -q "${APP}/${1}.php" -P "$SCRIPTS" || wget -q "${APP}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${APP}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

  # download_gitlab_script name_of_script
  download_gitlab_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

printf "%s\n""\e[0;32m" && echo "Updateing scripts from the web..." && printf "\e[0m\n"
# Most of the shell scripts in static (.sh)
download_static_script security
download_static_script update
download_static_script trusted
download_static_script setup_secure_permissions_nextcloud
download_static_script change_mysql_pass
download_static_script nextcloud
download_static_script update-config
download_static_script index
download_static_script history

# Most of the shell scripts in app (.sh)
download_app_script collabora
download_app_script nextant
download_app_script passman
download_app_script spreedme

# Most of the shell scripts in gitlab (.sh)
download_gitlab_script instruction
download_gitlab_script timezone
download_gitlab_script change-NCADMIN-password
download_gitlab_script fail2ban
download_gitlab_script sync-all-scripts
download_gitlab_script nextcloud-first-run-startup
download_gitlab_script nextcloud-every-startup

# Get temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi

printf "%s\n""\e[0;32m" && echo "Securing permissions" && printf "\e[0m\n"
# Make $SCRIPTS excutable
chmod +x -R "$SCRIPTS"
chown root:root -R "$SCRIPTS"

# Secure permissions
bash $SECURE & spinner_loading
