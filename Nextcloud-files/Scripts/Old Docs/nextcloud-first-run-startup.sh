#!/bin/bash
# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Requirements:
Install-Nextcloud-on-Ubuntu-16.04.sh executed successfully

# Pre-Config
WWW_ROOT=/var/www
NCPATH=$WWW_ROOT/nextcloud
NCDATA=/var/ncdata
SCRIPTS=/var/scripts
IFACE=$(lshw -c network | grep "logical name" | awk '{print $3; exit}')
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e `uname -r | cut -f1,2 -d"-"` | grep -e [0-9] | xargs sudo apt -y purge)
PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
GITHUB_REPO="https://raw.githubusercontent.com/nextcloud/vm/master"
STATIC="https://raw.githubusercontent.com/nextcloud/vm/master/static"
LETS_ENC="https://raw.githubusercontent.com/nextcloud/vm/master/lets-encrypt"
UNIXUSER=$SUDO_USER
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"
SSL_CONF="/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
HTTP_CONF="/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
NCREPO="https://download.nextcloud.com/server/releases/"
NCVERSION=$(curl -s $NCREPO | tac | grep unknown.gif | sed 's/.*"nextcloud-\([^"]*\).zip.sha512".*/\1/;q')
STABLEVERSION="nextcloud-$NCVERSION"
GPGDIR=/tmp/gpg
OpenPGP_fingerprint='28806A878AE423A28372792ED75899B9A724937A'

# Check where the best mirrors are and update
printf "\nTo make downloads as fast as possible when updating you should have mirrors that are as close to you as possible.\n"
echo "This VM comes with mirrors based on servers in that where used when the VM was released and packaged."
echo "We recomend you to change the mirrors based on where this is currently installed."
echo "Checking current mirror..."
printf "Your current server repository is:  ${Cyan}$REPO${Color_Off}\n"

if [[ "no" == $(ask_yes_or_no "Do you want to try to find a better mirror?") ]]
then
    echo "Keeping $REPO as mirror..."
    sleep 1
else
    echo "Locating the best mirrors..."
    apt update -q4 & spinner_loading
    apt install python-pip -y
    pip install \
        --upgrade pip \
        apt-select
    apt-select -m up-to-date -t 5 -c
    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \
    if [ -f sources.list ]
    then
        sudo mv sources.list /etc/apt/
    fi
fi

echo
echo "Updateing scripts from the web to be able to run the first setup..."

# Get most of the shell scripts in static (.sh)
download_static_script temporary-fix
download_static_script security
download_static_script update
download_static_script trusted
download_static_script nextcloud
download_static_script update-config
download_static_script index

# Activate SSL
if [ -f $SCRIPTS/activate-ssl.sh ]
then
    rm $SCRIPTS/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P $SCRIPTS
else
    wget -q $LETS_ENC/activate-ssl.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/activate-ssl.sh ]
then
    sleep 0.1
else
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Sets trusted domain in when nextcloud-first-run-startup.sh is finished
if [ -f $SCRIPTS/trusted.sh ]
then
    rm $SCRIPTS/trusted.sh
    wget -q $STATIC/trusted.sh -P $SCRIPTS
else
    wget -q $STATIC/trusted.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/trusted.sh ]
then
    sleep 0.1
else
    echo "trusted failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Sets secure permissions after upgrade
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    rm $SCRIPTS/Nextcloud_Set-File-Permissions.sh
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
else
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    sleep 0.1
else
    echo "setup_secure_permissions_nextcloud failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Change MySQL password
if [ -f $SCRIPTS/MySQLPasswdUpdate.sh ]
then
    rm $SCRIPTS/MySQLPasswdUpdate.sh
    wget -q $GITLAB/MySQLPasswdUpdate.sh
else
    wget -q $GITLAB/MySQLPasswdUpdate.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/MySQLPasswdUpdate.sh ]
then
    sleep 0.1
else
    echo "change_mysql_pass failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

clear
echo "+--------------------------------------------------------------------+"
echo "| This script will configure your Nextcloud and activate SSL.        |"
echo "| It will also do the following:                                     |"
echo "|                                                                    |"
echo "| - Generate new SSH keys for the server                             |"
echo "| - Generate new MySQL password                                      |"
echo "| - Configure UTF8mb4 (4-byte support for MySQL)                     |"
echo "| - Install phpMyadmin and make it secure                            |"
echo "| - Install selected apps and automatically configure them           |"
echo "| - Detect and set hostname                                          |"
echo "| - Upgrade your system and Nextcloud to latest version              |"
echo "| - Set secure permissions to Nextcloud                              |"
#echo "| - Set new passwords to Linux and Nextcloud                         |"
echo "| - Set new keyboard layout                                          |"
echo "| - Change timezone                                                  |"
#echo "| - Set static IP to the system (you have to set the same IP in      |"
#echo "|   your router) https://www.techandme.se/open-port-80-443/          |"
#echo "|   We don't set static IP if you run this on a *remote* VPS.        |"
echo "|                                                                    |"
echo "|   The script will take about 10 minutes to finish,                 |"
echo "|   depending on your internet connection.                           |"
echo "|                                                                    |"
echo "+--------------------------------------------------------------------+"
any_key "Press any key to start the script..."
clear

# Set keyboard layout
echo "Current keyboard layout is $(localectl status | grep "Layout" | awk '{print $3}')"
if [[ "no" == $(ask_yes_or_no "Do you want to change keyboard layout?") ]]
then
    echo "Not changing keyboard layout..."
    sleep 1
    clear
else
    dpkg-reconfigure keyboard-configuration
clear
fi

# Pretty URLs
echo "Setting RewriteBase to \"/\" in config.php..."
chown -R www-data:www-data $NCPATH
sudo -u www-data php $NCPATH/occ config:system:set htaccess.RewriteBase --value="/"
sudo -u www-data php $NCPATH/occ maintenance:update:htaccess
bash $SECURE & spinner_loading

# Generate new SSH Keys
printf "\nGenerating new SSH keys for the server...\n"
rm -v /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

# MySQL Setup & user passwd change
/etc/init.d/mysql stop
sleep 5
mysqld_safe --skip-grant-tables &
sleep 10
mysql -u root -e "use mysql;"
mysql -u root mysql -e "update user set password=PASSWORD("1") where User='root';"
mysql -u root -e "flush privileges;"
/etc/init.d/mysql stop
/etc/init.d/mysql start
mysql -u root -p1 -e "SET PASSWORD FOR root@localhost=PASSWORD('')"
mysql -u root -e "SET PASSWORD FOR root@127.0.0.1=PASSWORD('')"
mysql -u root -e "flush privileges;"
read -p "Type the password you want root to use for MySQL here: " NCPASS
mysqladmin -u root password $NCPASS
echo 'MySQL Password updated successfully for root user.'
mysql -u root mysql -p$NCPASS -e "update mysql.user set password=PASSWORD('$NCPASS') where User='oc_ncadmin';"
mysql -u root -p$NCPASS -e "flush privileges;"
echo 'MySQL Password updated successfully for oc_ncadmin user.'

# Set MySQL Variables
NCUSER=oc_ncadmin
NCPASS=$NEWPASSWD
NCDB=nextcloud_db

# Enable UTF8mb4 (4-byte support)
mysql -u root -p$NCPASS -e "ALTER DATABASE nextcloud_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
#sudo -u www-data $NCPATH/occ config:system:set mysql.utf8mb4 --type boolean --value="true"
#sudo -u www-data $NCPATH/occ maintenance:repair

# Install phpMyadmin
run_app_script phpmyadmin_install_ubuntu16
clear

cat << LETSENC
+-----------------------------------------------+
|  The following script will install a trusted  |
|  SSL certificate through Let's Encrypt.       |
+-----------------------------------------------+
LETSENC

# Let's Encrypt
if [[ "yes" == $(ask_yes_or_no "Do you want to install SSL?") ]]
then
    bash $SCRIPTS/activate-ssl.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/activate-ssl.sh"
    any_key "Press any key to continue..."
fi
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "Automatically configure and install selected apps\nSelect by pressing the spacebar" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Collabora" "(Online editing)   " OFF \
"Nextant" "(Full text search)   " OFF \
"Passman" "(Password storage)   " OFF \
"Spreed.ME" "(Video calls)   " OFF 2>results

while read -r -u 9 choice
do
    case $choice in
        Collabora)
            run_app_script collabora
        ;;

        Nextant)
            run_app_script nextant
        ;;

        Passman)
            run_app_script passman
        ;;

        Spreed.ME)
            run_app_script spreedme
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

# Add extra security
if [[ "yes" == $(ask_yes_or_no "Do you want to add extra security, based on this: http://goo.gl/gEJHi7 ?") ]]
then
    bash $SCRIPTS/security.sh
    rm "$SCRIPTS"/security.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/security.sh"
    any_key "Press any key to continue..."
fi
clear

# Change Timezone
echo "Current timezone is $(cat /etc/timezone)"
echo "You must change it to your timezone"
any_key "Press any key to change timezone..."
dpkg-reconfigure tzdata
sleep 3
clear

# Fixes https://github.com/nextcloud/vm/issues/58
a2dismod status
service apache2 reload

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
VALUE="# php_value upload_max_filesize 513M"
if ! grep -Fxq "$VALUE" $NCPATH/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 513M/# php_value upload_max_filesize 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value post_max_size 513M/# php_value post_max_size 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess
fi

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
#rm -f "$SCRIPTS/ip.sh"
#rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

# Upgrade system
echo "System will now upgrade..."
bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!
clear
printf "%s\n""${Green}"
echo    "+--------------------------------------------------------------------+"
echo    "|      Congratulations! You have successfully installed Nextcloud!   |"
echo    "|                                                                    |"
printf "|         ${Color_Off}Login to Nextcloud in your browser: ${Cyan}\"$ADDRESS2\"${Green}         |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}Publish your server online! ${Cyan}https://goo.gl/iUGE2U${Green}          |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}To login to MySQL just type: ${Cyan}'mysql -u root'${Green}               |\n"
echo    "|                                                                    |"
printf "|   ${Color_Off}To update this VM just type: ${Cyan}'sudo bash /var/scripts/update.sh'${Green}  |\n"
echo    "|                                                                    |"
echo    "+--------------------------------------------------------------------+"
printf "${Color_Off}\n"

# Set trusted domain in config.php
bash "$SCRIPTS"/trusted.sh
rm -f "$SCRIPTS"/trusted.sh

# Prefer IPv6
sed -i "s|precedence ::ffff:0:0/96  100|#precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# Reboot
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
any_key "Installation finished, press any key to reboot system..."
reboot
