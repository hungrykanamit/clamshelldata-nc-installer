#!/bin/bash

# Clamshell Data, https://clamshelldata.com

/etc/init.d/mysql stop
sleep 5
mysqld_safe --skip-grant-tables &
sleep 10
mysql -u root -e "use mysql;"
mysql -u root mysql -e "update user set password=PASSWORD("1") where User='root';"
mysql -u root -e "flush privileges;"
/etc/init.d/mysql stop
/etc/init.d/mysql start
mysql -u root -p1 -e "SET PASSWORD FOR root@localhost=PASSWORD('')"
mysql -u root -e "SET PASSWORD FOR root@127.0.0.1=PASSWORD('')"
mysql -u root -e "flush privileges;"
read -p "Type the password you want root to use for MySQL here: " NCPASS
mysqladmin -u root password $NCPASS
echo 'MySQL Password updated successfully for root user.'
mysql -u root mysql -p$NCPASS -e "update mysql.user set password=PASSWORD('$NCPASS') where User='oc_ncadmin';"
mysql -u root -p$NCPASS -e "flush privileges;"
echo 'MySQL Password updated successfully for oc_ncadmin user.'
