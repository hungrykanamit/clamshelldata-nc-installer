#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

# Requirements:
#Install-Nextcloud-on-Ubuntu-16.04.sh executed successfully

# Set Variables
echo "Setting Variables"
  # Dirs
    WWW_ROOT=/var/www
    SCRIPTS=/var/scripts
    NCPATH=/var/www/nextcloud
    HTML=/var/www
    NCDATA=/var/ncdata
    SNAPDIR=/var/snap/spreedme
    GPGDIR=/tmp/gpg
    BACKUP=/var/NCBACKUP
  # Ubuntu OS
    DISTRO=$(lsb_release -sd | cut -d ' ' -f 2)
    OS=$(grep -ic "Ubuntu" /etc/issue.net)
  # Network
    [ ! -z "$FIRST_IFACE" ] && IFACE=$(lshw -c network | grep "logical name" | awk '{print $3; exit}')
    IFACE2=$(ip -o link show | awk '{print $2,$9}' | grep 'UP' | cut -d ':' -f 1)
    [ ! -z "$CHECK_CURRENT_REPO" ] && REPO=$(apt-get update | grep -m 1 Hit | awk '{ print $2}')
    ADDRESS=$(hostname -I | cut -d ' ' -f 1)
    WGET="/usr/bin/wget"
  # WANIP4=$(dig +short myip.opendns.com @resolver1.opendns.com) # as an alternative
    WANIP4=$(curl -s -m 5 ipinfo.io/ip)
    [ ! -z "$LOAD_IP6" ] && WANIP6=$(curl -s -k -m 7 https://6.ifcfg.me)
    IFCONFIG="/sbin/ifconfig"
    INTERFACES="/etc/network/interfaces"
    NETMASK=$($IFCONFIG | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
    GATEWAY=$(route -n|grep "UG"|grep -v "UGH"|cut -f 10 -d " ")
  # Repo
    GITHUB_REPO="https://raw.githubusercontent.com/nextcloud/vm/master"
    STATIC="$GITHUB_REPO/static"
    LETS_ENC="$GITHUB_REPO/lets-encrypt"
    APP="$GITHUB_REPO/apps"
    NCREPO="https://download.nextcloud.com/server/releases"
    ISSUES="https://github.com/nextcloud/vm/issues"
    GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"
  # User information
    NCPASS=nextcloud
    NCUSER=ncadmin
    UNIXUSER=$SUDO_USER
    UNIXUSER_PROFILE="/home/$UNIXUSER/.bash_profile"
    ROOT_PROFILE="/root/.bash_profile"
  # Passwords
    SHUF=$(shuf -i 25-29 -n 1)
    MYSQL_PASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$SHUF" | head -n 1)
    NEWMYSQLPASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$SHUF" | head -n 1)
  # Path to specific files
    PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
    SECURE="$SCRIPTS/setup_secure_permissions_nextcloud.sh"
    SSL_CONF="/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
    HTTP_CONF="/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
    PW_FILE=/var/mysql_password.txt
    MYCNF=/root/.my.cnf
    [ ! -z "$CHANGE_MYSQL" ] && OLDMYSQL=$(cat $PW_FILE)
  # Nextcloud version
    [ ! -z "$NC_UPDATE" ] && CURRENTVERSION=$(sudo -u www-data php $NCPATH/occ status | grep "versionstring" | awk '{print $3}')
    NCVERSION=$(curl -s -m 900 $NCREPO/ | tac | grep unknown.gif | sed 's/.*"nextcloud-\([^"]*\).zip.sha512".*/\1/;q')
    STABLEVERSION="nextcloud-$NCVERSION"
    NCMAJOR="${NCVERSION%%.*}"
    NCBAD=$((NCMAJOR-2))
  # Keys
    OpenPGP_fingerprint='28806A878AE423A28372792ED75899B9A724937A'
  # Collabora Docker URL
    [ ! -z "$COLLABORA_INSTALL" ] && SUBDOMAIN=$(whiptail --title "Techandme.se Collabora" --inputbox "Collabora subdomain eg: office.yourdomain.com" "$WT_HEIGHT" "$WT_WIDTH" 3>&1 1>&2 2>&3)
  # Nextcloud Main Domain
    [ ! -z "$COLLABORA_INSTALL" ] && NCDOMAIN=$(whiptail --title "Techandme.se Collabora" --inputbox "Nextcloud url, make sure it looks like this: cloud\\.yourdomain\\.com" "$WT_HEIGHT" "$WT_WIDTH" cloud\\.yourdomain\\.com 3>&1 1>&2 2>&3)
  # Letsencrypt
    LETSENCRYPTPATH="/etc/letsencrypt"
    CERTFILES="$LETSENCRYPTPATH/live"
    DHPARAMS="$CERTFILES/$SUBDOMAIN/dhparam.pem"
  # Collabora App
    [ ! -z "$COLLABORA_INSTALL" ] && COLLVER=$(curl -s https://api.github.com/repos/nextcloud/richdocuments/releases/latest | grep "tag_name" | cut -d\" -f4)
    COLLVER_FILE=richdocuments.tar.gz
    COLLVER_REPO=https://github.com/nextcloud/richdocuments/releases/download
    HTTPS_CONF="/etc/apache2/sites-available/$SUBDOMAIN.conf"
  # Nextant
    SOLR_VERSION=$(curl -s https://github.com/apache/lucene-solr/tags | grep -o "release.*</span>$" | grep -o '[0-9].[0-9].[0-9]' | sort -t. -k1,1n -k2,2n -k3,3n | tail -n1)
    [ ! -z "$NEXTANT_INSTALL" ] && NEXTANT_VERSION=$(curl -s https://api.github.com/repos/nextcloud/nextant/releases/latest | grep 'tag_name' | cut -d\" -f4 | sed -e "s|v||g")
    NT_RELEASE=nextant-master-$NEXTANT_VERSION.tar.gz
    NT_DL=https://github.com/nextcloud/nextant/releases/download/v$NEXTANT_VERSION/$NT_RELEASE
    SOLR_RELEASE=solr-$SOLR_VERSION.tgz
    SOLR_DL=http://www-eu.apache.org/dist/lucene/solr/$SOLR_VERSION/$SOLR_RELEASE
    NC_APPS_PATH=$NCPATH/apps/
    SOLR_HOME=/home/$SUDO_USER/solr_install/
    SOLR_JETTY=/opt/solr/server/etc/jetty-http.xml
    SOLR_DSCONF=/opt/solr-$SOLR_VERSION/server/solr/configsets/data_driven_schema_configs/conf/solrconfig.xml
  # Passman
    [ ! -z "$PASSMAN_INSTALL" ] && PASSVER=$(curl -s https://api.github.com/repos/nextcloud/passman/releases/latest | grep "tag_name" | cut -d\" -f4)
    PASSVER_FILE=passman_$PASSVER.tar.gz
    PASSVER_REPO=https://releases.passman.cc
    SHA256=/tmp/sha256
  # Calendar
    [ ! -z "$CALENDAR_INSTALL" ] && CALVER=$(curl -s https://api.github.com/repos/nextcloud/calendar/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
    CALVER_FILE=calendar.tar.gz
    CALVER_REPO=https://github.com/nextcloud/calendar/releases/download
  # Contacts
    [ ! -z "$CONTACTS_INSTALL" ] && CONVER=$(curl -s https://api.github.com/repos/nextcloud/contacts/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
    CONVER_FILE=contacts.tar.gz
    CONVER_REPO=https://github.com/nextcloud/contacts/releases/download
  # Spreed.ME
    SPREEDME_VER=$(wget -q https://raw.githubusercontent.com/strukturag/nextcloud-spreedme/master/appinfo/info.xml && grep -Po "(?<=<version>)[^<]*(?=</version>)" info.xml && rm info.xml)
    SPREEDME_FILE="v$SPREEDME_VER.tar.gz"
    SPREEDME_REPO=https://github.com/strukturag/nextcloud-spreedme/archive
  # phpMyadmin
    PHPMYADMINDIR=/usr/share/phpmyadmin
    PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
    UPLOADPATH=""
    SAVEPATH=""
  # Redis
    REDIS_CONF=/etc/redis/redis.conf
    REDIS_SOCK=/var/run/redis/redis.sock
    RSHUF=$(shuf -i 30-35 -n 1)
    REDIS_PASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$RSHUF" | head -n 1)
  # Extra security
    CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e `uname -r | cut -f1,2 -d"-"` | grep -e [0-9] | xargs sudo apt -y purge)
    SPAMHAUS=/etc/spamhaus.wl
    ENVASIVE=/etc/apache2/mods-available/mod-evasive.load
    APACHE2=/etc/apache2/apache2.conf

# Set Functions
echo "Setting Functions"
  # ask_yes_or_no
    ask_yes_or_no() {
        read -r -p "$1 ([y]es or [N]o): "
        case ${REPLY,,} in
            y|yes)
                echo "yes"
            ;;
            *)
                echo "no"
            ;;
        esac
    }

  # check_command
    check_command() {
      eval "$*"
      if [ ! $? -eq 0 ]; then
         printf "${IRed}Sorry but something went wrong. Please report this issue to $ISSUES and include the output of the error message. Thank you!${Color_Off}\n"
         echo "$* failed"
        exit 1
      fi
    }

  # network_ok
    network_ok() {
        echo "Testing if network is OK..."
        service networking restart
        if wget -q -T 20 -t 2 http://github.com -O /dev/null & spinner_loading
        then
            return 0
        else
            return 1
        fi
    }

  # Whiptail auto-size
    calc_wt_size() {
        WT_HEIGHT=17
        WT_WIDTH=$(tput cols)

        if [ -z "$WT_WIDTH" ] || [ "$WT_WIDTH" -lt 60 ]; then
            WT_WIDTH=80
        fi
        if [ "$WT_WIDTH" -gt 178 ]; then
            WT_WIDTH=120
        fi
        WT_MENU_HEIGHT=$((WT_HEIGHT-7))
        export WT_MENU_HEIGHT
    }

  # download_verify_nextcloud_stable
    download_verify_nextcloud_stable() {
    wget -q -T 10 -t 2 "$NCREPO/$STABLEVERSION.tar.bz2" -P "$HTML"
    mkdir -p "$GPGDIR"
    wget -q "$NCREPO/$STABLEVERSION.tar.bz2.asc" -P "$GPGDIR"
    chmod -R 600 "$GPGDIR"
    gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$OpenPGP_fingerprint"
    gpg --verify "$GPGDIR/$STABLEVERSION.tar.bz2.asc" "$HTML/$STABLEVERSION.tar.bz2"
    rm -r "$GPGDIR"
    }

  # Initial download of script in ../static
    # call like: download_static_script name_of_script
    download_static_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if ! { wget -q "${STATIC}/${1}.sh" -P "$SCRIPTS" || wget -q "${STATIC}/${1}.php" -P "$SCRIPTS" || wget -q "${STATIC}/${1}.py" -P "$SCRIPTS"; }
        then
            echo "{$1} failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|.php|.py' again."
            echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
            echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
            exit 1
        fi
    }

  # Initial download of script in ../lets-encrypt
    # call like: download_le_script name_of_script
    download_le_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if ! { wget -q "${LETS_ENC}/${1}.sh" -P "$SCRIPTS" || wget -q "${LETS_ENC}/${1}.php" -P "$SCRIPTS" || wget -q "${LETS_ENC}/${1}.py" -P "$SCRIPTS"; }
        then
            echo "{$1} failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|.php|.py' again."
            echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
            echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
            exit 1
        fi
    }

  # Run any script in ../static
    # call like: run_static_script name_of_script
    run_static_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if wget -q "${STATIC}/${1}.sh" -P "$SCRIPTS"
        then
            bash "${SCRIPTS}/${1}.sh"
            rm -f "${SCRIPTS}/${1}.sh"
        elif wget -q "${STATIC}/${1}.php" -P "$SCRIPTS"
        then
            php "${SCRIPTS}/${1}.php"
            rm -f "${SCRIPTS}/${1}.php"
        elif wget -q "${STATIC}/${1}.py" -P "$SCRIPTS"
        then
            python "${SCRIPTS}/${1}.py"
            rm -f "${SCRIPTS}/${1}.py"
        else
            echo "Downloading ${1} failed"
            echo "Script failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|php|py' again."
            sleep 3
        fi
    }

  # Run any script in ../apps
    # call like: run_app_script collabora|nextant|passman|spreedme|contacts|calendar|webmin
    run_app_script() {
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if wget -q "${APP}/${1}.sh" -P "$SCRIPTS"
        then
            bash "${SCRIPTS}/${1}.sh"
            rm -f "${SCRIPTS}/${1}.sh"
        elif wget -q "${APP}/${1}.php" -P "$SCRIPTS"
        then
            php "${SCRIPTS}/${1}.php"
            rm -f "${SCRIPTS}/${1}.php"
        elif wget -q "${APP}/${1}.py" -P "$SCRIPTS"
        then
            python "${SCRIPTS}/${1}.py"
            rm -f "${SCRIPTS}/${1}.py"
        else
            echo "Downloading ${1} failed"
            echo "Script failed to download. Please run: 'sudo wget ${APP}/${1}.sh|php|py' again."
            sleep 3
        fi
    }

  # version
    version(){
        local h t v

        [[ $2 = "$1" || $2 = "$3" ]] && return 0

        v=$(printf '%s\n' "$@" | sort -V)
        h=$(head -n1 <<<"$v")
        t=$(tail -n1 <<<"$v")

        [[ $2 != "$h" && $2 != "$t" ]]
    }

  # version_gt
    version_gt() {
        local v1 v2 IFS=.
        read -ra v1 <<< "$1"
        read -ra v2 <<< "$2"
        printf -v v1 %03d "${v1[@]}"
        printf -v v2 %03d "${v2[@]}"
        [[ $v1 > $v2 ]]
    }

  # spinner_loading
    spinner_loading() {
        pid=$!
        spin='-\|/'
        i=0
        while kill -0 $pid 2>/dev/null
        do
            i=$(( (i+1) %4 ))
            printf "\r[${spin:$i:1}] " # Add text here, something like "Please be paitent..." maybe?
            sleep .1
        done
    }

  # any_key
    any_key() {
        local PROMPT="$1"
        read -r -p "$(printf "${Green}${PROMPT}${Color_Off}")" -n1 -s
        echo
    }

# Set BASH Colors
echo "Setting Colors"
  # Reset
    Color_Off='\e[0m'       # Text Reset

  # Regular Colors
    Black='\e[0;30m'        # Black
    Red='\e[0;31m'          # Red
    Green='\e[0;32m'        # Green
    Yellow='\e[0;33m'       # Yellow
    Blue='\e[0;34m'         # Blue
    Purple='\e[0;35m'       # Purple
    Cyan='\e[0;36m'         # Cyan
    White='\e[0;37m'        # White

  # Bold
    BBlack='\e[1;30m'       # Black
    BRed='\e[1;31m'         # Red
    BGreen='\e[1;32m'       # Green
    BYellow='\e[1;33m'      # Yellow
    BBlue='\e[1;34m'        # Blue
    BPurple='\e[1;35m'      # Purple
    BCyan='\e[1;36m'        # Cyan
    BWhite='\e[1;37m'       # White

  # Underline
    UBlack='\e[4;30m'       # Black
    URed='\e[4;31m'         # Red
    UGreen='\e[4;32m'       # Green
    UYellow='\e[4;33m'      # Yellow
    UBlue='\e[4;34m'        # Blue
    UPurple='\e[4;35m'      # Purple
    UCyan='\e[4;36m'        # Cyan
    UWhite='\e[4;37m'       # White

  # Background
    On_Black='\e[40m'       # Black
    On_Red='\e[41m'         # Red
    On_Green='\e[42m'       # Green
    On_Yellow='\e[43m'      # Yellow
    On_Blue='\e[44m'        # Blue
    On_Purple='\e[45m'      # Purple
    On_Cyan='\e[46m'        # Cyan
    On_White='\e[47m'       # White

  # High Intensity
    IBlack='\e[0;90m'       # Black
    IRed='\e[0;91m'         # Red
    IGreen='\e[0;92m'       # Green
    IYellow='\e[0;93m'      # Yellow
    IBlue='\e[0;94m'        # Blue
    IPurple='\e[0;95m'      # Purple
    ICyan='\e[0;96m'        # Cyan
    IWhite='\e[0;97m'       # White

  # Bold High Intensity
    BIBlack='\e[1;90m'      # Black
    BIRed='\e[1;91m'        # Red
    BIGreen='\e[1;92m'      # Green
    BIYellow='\e[1;93m'     # Yellow
    BIBlue='\e[1;94m'       # Blue
    BIPurple='\e[1;95m'     # Purple
    BICyan='\e[1;96m'       # Cyan
    BIWhite='\e[1;97m'      # White

  # High Intensity backgrounds
    On_IBlack='\e[0;100m'   # Black
    On_IRed='\e[0;101m'     # Red
    On_IGreen='\e[0;102m'   # Green
    On_IYellow='\e[0;103m'  # Yellow
    On_IBlue='\e[0;104m'    # Blue
    On_IPurple='\e[0;105m'  # Purple
    On_ICyan='\e[0;106m'    # Cyan
    On_IWhite='\e[0;107m'   # White

## Check where the best mirrors are and update (Skipped, not essential and buggy)
#printf "\nTo make downloads as fast as possible when updating you should have mirrors that are as close to you as possible.\n"
#echo "This VM comes with mirrors based on servers in that where used when the VM was released and packaged."
#echo "We recomend you to change the mirrors based on where this is currently installed."
#echo "Checking current mirror..."
#printf "Your current server repository is:  ${Cyan}$REPO${Color_Off}\n"

#if [[ "no" == $(ask_yes_or_no "Do you want to try to find a better mirror?") ]]
#then
#    echo "Keeping $REPO as mirror..."
#    sleep 1
#else
#    echo "Locating the best mirrors..."
#    apt update -q4 & spinner_loading
#    apt install python-pip -y
#    pip install \
#        --upgrade pip \
#        apt-select
#    apt-select -m up-to-date -t 5 -c
#    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \
#    if [ -f sources.list ]
#    then
#        sudo mv sources.list /etc/apt/
#    fi
#fi

any_key "This is a test to see if the script starts after reboot, press any key to continue"

echo
echo "Updateing scripts from the web to be able to run the first setup..."
# All the shell scripts in static (.sh)
#download_static_script temporary-fix
wget -q https://raw.githubusercontent.com/nextcloud/vm/master/static/temporary-fix.sh -P $SCRIPTS
download_static_script security
download_static_script update
download_static_script trusted
download_static_script nextcloud
download_static_script update-config
download_static_script index

# Update timezone.sh
if [ -f ${SCRIPTS}/timezone.sh ]
then
    rm ${SCRIPTS}/timezone.sh
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
else
    wget -q "${GITLAB}/timezone.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

# Sets secure permissions after upgrade
echo "Setting Permissions"
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    rm $SCRIPTS/Nextcloud_Set-File-Permissions.sh
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
else
    wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
fi
if [ -f $SCRIPTS/Nextcloud_Set-File-Permissions.sh ]
then
    sleep 0.1
else
    echo "setup_secure_permissions_nextcloud failed"
    echo "Script failed to download. Please run: 'sudo bash /var/scripts/nextcloud-first-run-startup.sh' again."
    exit 1
fi

# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

clear
echo "+--------------------------------------------------------------------+"
echo "| This script will configure your Nextcloud and activate SSL.        |"
echo "| It will also do the following:                                     |"
echo "|                                                                    |"
echo "| - Set new keyboard layout                                          |"
echo "| - Change timezone                                                  |"
echo "| - Generate new SSH keys for the server                             |"
echo "| - Generate new MySQL password                                      |"
echo "| - Configure UTF8mb4 (4-byte support for MySQL)                     |"
echo "| - Install phpMyadmin and make it secure                            |"
echo "| - Install selected apps and automatically configure them           |"
echo "| - Detect and set hostname                                          |"
echo "| - Upgrade your system and Nextcloud to latest version              |"
echo "| - Set secure permissions to Nextcloud                              |"
#echo "| - Set new passwords to Linux and Nextcloud                         |"
#echo "| - Set static IP to the system (you have to set the same IP in      |"
#echo "|   your router) https://www.techandme.se/open-port-80-443/          |"
#echo "|   We don't set static IP if you run this on a *remote* VPS.        |"
echo "|                                                                    |"
echo "|   The script will take about 10 minutes to finish,                 |"
echo "|   depending on your internet connection.                           |"
echo "|                                                                    |"
echo "+--------------------------------------------------------------------+"
any_key "Press any key to start the script..."
clear

# Set keyboard layout (Skiped, not essential)
#echo "Current keyboard layout is $(localectl status | grep "Layout" | awk '{print $3}')"
#if [[ "no" == $(ask_yes_or_no "Do you want to change keyboard layout?") ]]
#then
#    echo "Not changing keyboard layout..."
#    sleep 1
#    clear
#else
#    dpkg-reconfigure keyboard-configuration
#clear
#fi

# Change Timezone
if [[ "yes" == $(ask_yes_or_no "Do you want to change your timezone?") ]]
then
	dpkg-reconfigure tzdata
	sleep 3
  any_key "Press any key to continue..."
else
    echo
    echo "OK, Current timezone is $(cat /etc/timezone)"
    echo "However, if you want to change it later, just type: sudo bash $SCRIPTS/timezone.sh"
    any_key "Press any key to continue..."
fi
clear

# Pretty URLs
echo "Setting RewriteBase to \"/\" in config.php..."
chown -R www-data:www-data $NCPATH
sudo -u www-data php $NCPATH/occ config:system:set htaccess.RewriteBase --value="/"
sudo -u www-data php $NCPATH/occ maintenance:update:htaccess
bash $SECURE & spinner_loading

# Generate new SSH Keys
printf "\nGenerating new SSH keys for the server...\n"
rm -v /etc/ssh/ssh_host_*
dpkg-reconfigure openssh-server

#mysql_secure_installation
apt -y update
apt -y install expect
read -p "Type the password you want root to use for MySQL here: " NCPASS
echo "MySQL is being secured, this may take a moment..." & spinner_loading
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
send \"nextcloud\r\"
#expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
#send \"n\r\"
expect \"Change the password for root ?\"
send \"y\r\"
send \"$NCPASS\r\"
send \"$NCPASS\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL"
apt -y purge expect

# Set MySQL Variables
#NCUSER=oc_ncadmin
#NCDB=nextcloud_db

# MySQL oc_ncadmin passwd change
mysql -u root mysql -p$NCPASS -e "update mysql.user set password=PASSWORD('$NCPASS') where User='oc_ncadmin';"
mysql -u root -p$NCPASS -e "flush privileges;"

# Enable UTF8mb4 (4-byte support) [this has been moved to Install-Nextcloud-on-Ubuntu-16.04.sh as it might as well be enabled be default]
echo "UTF8mb has already been enabled"
#echo "Configure UTF8mb4 (4-byte support for MySQL)"
#mysql -u root -p$NCPASS -e "ALTER DATABASE nextcloud_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
#sudo -u www-data $NCPATH/occ config:system:set mysql.utf8mb4 --type boolean --value="true"
#sudo -u www-data $NCPATH/occ maintenance:repair
# Change InnoDB settings for Mariadb
#sed -i "/binlog_format = 'MIXED'/ a innodb_large_prefix=true" /etc/mysql/my.cnf
#sed -i "/innodb_large_prefix=true/ a innodb_file_format=barracuda" /etc/mysql/my.cnf
#sed -i "/innodb_file_format=barracuda/ a innodb_file_per_table=1" /etc/mysql/my.cnf

sudo /etc/init.d/mysql restart & spinner_loading


echo " "
echo "MySQL has been secured and the passwords for root & oc-ncadmin have been set to: $NCPASS"
echo "Please make a record of this"
any_key "Press any key to continue..."

# Install phpMyadmin
run_app_script phpmyadmin_install_ubuntu16
clear

cat << LETSENC
+-----------------------------------------------+
|  The following script will install a trusted  |
|  SSL certificate through Lets Encrypt.        |
+-----------------------------------------------+
LETSENC

# Lets Encrypt
if [[ "yes" == $(ask_yes_or_no "Do you want to install SSL?") ]]
then
    bash $SCRIPTS/activate-ssl.sh
else
    echo
    echo "OK, but please note that Collabora will not work without ssl being enabled"
    echo "If you want to run this script later, just type: sudo bash $SCRIPTS/activate-ssl.sh"
    any_key "Press any key to continue..."
fi
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "Automatically configure and install selected apps\nSelect by pressing the spacebar" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Collabora" "(Online editing)   " OFF \
"Passman" "(Password storage)   " OFF \
"Spreed.ME" "(Video calls)   " OFF 2>results
#"Nextant" "(Full text search)   " OFF \

while read -r -u 9 choice
do
    case $choice in
        Collabora)
            run_app_script collabora
        ;;

        Nextant)
            run_app_script nextant
        ;;

        Passman)
            run_app_script passman
        ;;

        Spreed.ME)
            run_app_script spreedme
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

# Add extra security
if [[ "yes" == $(ask_yes_or_no "Do you want to add extra security, based on this: http://goo.gl/gEJHi7 ?") ]]
then
    bash $SCRIPTS/security.sh
    rm "$SCRIPTS"/security.sh
else
    echo
    echo "OK, but if you want to run it later, just type: sudo bash $SCRIPTS/security.sh"
    any_key "Press any key to continue..."
fi
clear

## Change password (Skipped)
#printf "${Color_Off}\n"
#echo "For better security, change the system user password for [$UNIXUSER]"
#any_key "Press any key to change password for system user..."
#while true
#do
#    sudo passwd "$UNIXUSER" && break
#done
#echo
#clear
#NCADMIN=$(sudo -u www-data php $NCPATH/occ user:list | awk '{print $3}')
#printf "${Color_Off}\n"
#echo "For better security, change the Nextcloud password for [$NCADMIN]"
#echo "The current password for $NCADMIN is [$NCPASS]"
#any_key "Press any key to change password for Nextcloud..."
#while true
#do
#    sudo -u www-data php "$NCPATH/occ" user:resetpassword "$NCADMIN" && break
#done
#clear

# Fixes https://github.com/nextcloud/vm/issues/58
a2dismod status
service apache2 reload

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
VALUE="# php_value upload_max_filesize 513M"
if ! grep -Fxq "$VALUE" $NCPATH/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 513M/# php_value upload_max_filesize 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value post_max_size 513M/# php_value post_max_size 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess
fi

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Replace old salted password with plain text password in /var/www/nextcloud/config/config.php (this is not the ideal situation, research should be done on this)
sed -i '/dbpassword/d' /var/www/nextcloud/config/config.php
sed -i "/'dbuser' => 'oc_ncadmin',/ a 'dbpassword' => '$NCPASS'," /var/www/nextcloud/config/config.php

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
#rm -f "$SCRIPTS/ip.sh"
#rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' -o -name '*.sql*' \) -delete

#not working, need to fix
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

    sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
    cat << RCLOCAL > "/etc/rc.local"
    #!/bin/sh -e
    #
    # rc.local
    #
    # This script is executed at the end of each multiuser runlevel.
    # Make sure that the script will "exit 0" on success or any other
    # value on error.
    #
    # In order to enable or disable this script just change the execution
    # bits.
    #
    # By default this script does nothing.

    exit 0

    RCLOCAL
    clear

# Upgrade system
    echo "System will now upgrade..."
    bash $SCRIPTS/update.sh

# Cleanup 2
    apt autoremove -y
    apt autoclean
    rm -rf /var/cache/apt/archives/*.deb
    CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
    echo "$CLEARBOOT"

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!
clear

printf "%s\n""${Green}"
echo    "+--------------------------------------------------------------------+"
echo    "|      Congratulations! You have successfully installed Nextcloud!   |"
echo    "|                                                                    |"
printf "|         ${Color_Off}Login to Nextcloud in your browser: ${Cyan}\"$ADDRESS2\"${Green}         |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}Publish your server online! ${Cyan}https://goo.gl/iUGE2U${Green}          |\n"
echo    "|                                                                    |"
printf "|         ${Color_Off}To login to MySQL just type: ${Cyan}'mysql -u root'${Green}               |\n"
echo    "|                                                                    |"
printf "|   ${Color_Off}To update this server just type: ${Cyan}'sudo bash /var/scripts/update.sh'${Green}  |\n"
echo    "|                                                                    |"
echo    "+--------------------------------------------------------------------+"
printf "${Color_Off}\n"

# Set trusted domain in config.php
bash "$SCRIPTS"/trusted.sh
rm -f "$SCRIPTS"/trusted.sh

# Prefer IPv6
sed -i "s|precedence ::ffff:0:0/96  100|#precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# Reboot
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
any_key "Installation finished, press any key to reboot system..."
reboot
