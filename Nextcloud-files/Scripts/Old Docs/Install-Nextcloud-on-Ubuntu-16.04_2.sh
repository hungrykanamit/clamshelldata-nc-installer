#!/bin/bash

# Notes
##As of writing this (04/08/2017) mariadb would not install on ubuntu 16.04.2 so to do the install I ran a clean install of ubuntu 14.04 and ran the following:
#apt-get -y update
#apt-get -y upgrade
#apt-get -y install apache2 apache2-utils
#sudo service apache2 start
#sudo service apache2 status
#apt-get -y install software-properties-common
#sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
#sudo add-apt-repository 'deb http://download.nus.edu.sg/mirror/mariadb/repo/10.0/ubuntu trusty main'
#apt-get -y update
#apt-get -y install mariadb-server
##apt-get -y install mysql-server-5.6 mysql-client-5.6
#sudo service mysql start
#sudo service mysql status
#rm -rf /tmp/*
#rm -rf /var/cache/apt/archives/*.deb
#apt-get -y autoremove
#apt-get -y autoclean
#apt-get -y update
#apt-get dist-upgrade
#apt-get install update-manager-core
#do-release-upgrade
##REBOOT WHEN PROMPTED
#rm -rf /tmp/*
#rm -rf /var/cache/apt/archives/*.deb
#apt-get -y autoremove
#apt-get -y autoclean
#sudo service apache2 start
#sudo service apache2 status
#sudo service mysql start
#sudo service mysql status

# Requirements:
#ubuntu 16.04
#512 MB Ram
#4 GB Disk
#Mariadb 10.0 (Not 10.1)

# Set Variables
echo "Setting Variables"
  # Dirs
    WWW_ROOT=/var/www
    SCRIPTS=/var/scripts
    NCPATH=/var/www/nextcloud
    HTML=/var/www
    NCDATA=/var/ncdata
    SNAPDIR=/var/snap/spreedme
    GPGDIR=/tmp/gpg
    BACKUP=/var/NCBACKUP
  # Ubuntu OS
    DISTRO=$(lsb_release -sd | cut -d ' ' -f 2)
    OS=$(grep -ic "Ubuntu" /etc/issue.net)
  # Network
    [ ! -z "$FIRST_IFACE" ] && IFACE=$(lshw -c network | grep "logical name" | awk '{print $3; exit}')
    IFACE2=$(ip -o link show | awk '{print $2,$9}' | grep 'UP' | cut -d ':' -f 1)
    [ ! -z "$CHECK_CURRENT_REPO" ] && REPO=$(apt-get update | grep -m 1 Hit | awk '{ print $2}')
    ADDRESS=$(hostname -I | cut -d ' ' -f 1)
    WGET="/usr/bin/wget"
  # WANIP4=$(dig +short myip.opendns.com @resolver1.opendns.com) # as an alternative
    WANIP4=$(curl -s -m 5 ipinfo.io/ip)
    [ ! -z "$LOAD_IP6" ] && WANIP6=$(curl -s -k -m 7 https://6.ifcfg.me)
    IFCONFIG="/sbin/ifconfig"
    INTERFACES="/etc/network/interfaces"
    NETMASK=$($IFCONFIG | grep -w inet |grep -v 127.0.0.1| awk '{print $4}' | cut -d ":" -f 2)
    GATEWAY=$(route -n|grep "UG"|grep -v "UGH"|cut -f 10 -d " ")
  # Repo
    GITHUB_REPO="https://raw.githubusercontent.com/nextcloud/vm/master"
    STATIC="$GITHUB_REPO/static"
    LETS_ENC="$GITHUB_REPO/lets-encrypt"
    APP="$GITHUB_REPO/apps"
    NCREPO="https://download.nextcloud.com/server/releases"
    ISSUES="https://github.com/nextcloud/vm/issues"
    GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"
  # User information
    NCPASS=nextcloud
    NCUSER=ncadmin
    UNIXUSER=$SUDO_USER
    UNIXUSER_PROFILE="/home/$UNIXUSER/.bash_profile"
    ROOT_PROFILE="/root/.bash_profile"
  # Passwords
    SHUF=$(shuf -i 25-29 -n 1)
    MYSQL_PASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$SHUF" | head -n 1)
    NEWMYSQLPASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$SHUF" | head -n 1)
  # Path to specific files
    PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
    SECURE="$SCRIPTS/setup_secure_permissions_nextcloud.sh"
    SSL_CONF="/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
    HTTP_CONF="/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
    PW_FILE=/var/mysql_password.txt
    MYCNF=/root/.my.cnf
    [ ! -z "$CHANGE_MYSQL" ] && OLDMYSQL=$(cat $PW_FILE)
  # Nextcloud version
    [ ! -z "$NC_UPDATE" ] && CURRENTVERSION=$(sudo -u www-data php $NCPATH/occ status | grep "versionstring" | awk '{print $3}')
    NCVERSION=$(curl -s -m 900 $NCREPO/ | tac | grep unknown.gif | sed 's/.*"nextcloud-\([^"]*\).zip.sha512".*/\1/;q')
    STABLEVERSION="nextcloud-$NCVERSION"
    NCMAJOR="${NCVERSION%%.*}"
    NCBAD=$((NCMAJOR-2))
  # Keys
    OpenPGP_fingerprint='28806A878AE423A28372792ED75899B9A724937A'
  # Collabora Docker URL
    [ ! -z "$COLLABORA_INSTALL" ] && SUBDOMAIN=$(whiptail --title "Techandme.se Collabora" --inputbox "Collabora subdomain eg: office.yourdomain.com" "$WT_HEIGHT" "$WT_WIDTH" 3>&1 1>&2 2>&3)
  # Nextcloud Main Domain
    [ ! -z "$COLLABORA_INSTALL" ] && NCDOMAIN=$(whiptail --title "Techandme.se Collabora" --inputbox "Nextcloud url, make sure it looks like this: cloud\\.yourdomain\\.com" "$WT_HEIGHT" "$WT_WIDTH" cloud\\.yourdomain\\.com 3>&1 1>&2 2>&3)
  # Letsencrypt
    LETSENCRYPTPATH="/etc/letsencrypt"
    CERTFILES="$LETSENCRYPTPATH/live"
    DHPARAMS="$CERTFILES/$SUBDOMAIN/dhparam.pem"
  # Collabora App
    [ ! -z "$COLLABORA_INSTALL" ] && COLLVER=$(curl -s https://api.github.com/repos/nextcloud/richdocuments/releases/latest | grep "tag_name" | cut -d\" -f4)
    COLLVER_FILE=richdocuments.tar.gz
    COLLVER_REPO=https://github.com/nextcloud/richdocuments/releases/download
    HTTPS_CONF="/etc/apache2/sites-available/$SUBDOMAIN.conf"
  # Nextant
    SOLR_VERSION=$(curl -s https://github.com/apache/lucene-solr/tags | grep -o "release.*</span>$" | grep -o '[0-9].[0-9].[0-9]' | sort -t. -k1,1n -k2,2n -k3,3n | tail -n1)
    [ ! -z "$NEXTANT_INSTALL" ] && NEXTANT_VERSION=$(curl -s https://api.github.com/repos/nextcloud/nextant/releases/latest | grep 'tag_name' | cut -d\" -f4 | sed -e "s|v||g")
    NT_RELEASE=nextant-master-$NEXTANT_VERSION.tar.gz
    NT_DL=https://github.com/nextcloud/nextant/releases/download/v$NEXTANT_VERSION/$NT_RELEASE
    SOLR_RELEASE=solr-$SOLR_VERSION.tgz
    SOLR_DL=http://www-eu.apache.org/dist/lucene/solr/$SOLR_VERSION/$SOLR_RELEASE
    NC_APPS_PATH=$NCPATH/apps/
    SOLR_HOME=/home/$SUDO_USER/solr_install/
    SOLR_JETTY=/opt/solr/server/etc/jetty-http.xml
    SOLR_DSCONF=/opt/solr-$SOLR_VERSION/server/solr/configsets/data_driven_schema_configs/conf/solrconfig.xml
  # Passman
    [ ! -z "$PASSMAN_INSTALL" ] && PASSVER=$(curl -s https://api.github.com/repos/nextcloud/passman/releases/latest | grep "tag_name" | cut -d\" -f4)
    PASSVER_FILE=passman_$PASSVER.tar.gz
    PASSVER_REPO=https://releases.passman.cc
    SHA256=/tmp/sha256
  # Calendar
    [ ! -z "$CALENDAR_INSTALL" ] && CALVER=$(curl -s https://api.github.com/repos/nextcloud/calendar/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
    CALVER_FILE=calendar.tar.gz
    CALVER_REPO=https://github.com/nextcloud/calendar/releases/download
  # Contacts
    [ ! -z "$CONTACTS_INSTALL" ] && CONVER=$(curl -s https://api.github.com/repos/nextcloud/contacts/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
    CONVER_FILE=contacts.tar.gz
    CONVER_REPO=https://github.com/nextcloud/contacts/releases/download
  # Spreed.ME
    SPREEDME_VER=$(wget -q https://raw.githubusercontent.com/strukturag/nextcloud-spreedme/master/appinfo/info.xml && grep -Po "(?<=<version>)[^<]*(?=</version>)" info.xml && rm info.xml)
    SPREEDME_FILE="v$SPREEDME_VER.tar.gz"
    SPREEDME_REPO=https://github.com/strukturag/nextcloud-spreedme/archive
  # phpMyadmin
    PHPMYADMINDIR=/usr/share/phpmyadmin
    PHPMYADMIN_CONF="/etc/apache2/conf-available/phpmyadmin.conf"
    UPLOADPATH=""
    SAVEPATH=""
  # Redis
    REDIS_CONF=/etc/redis/redis.conf
    REDIS_SOCK=/var/run/redis/redis.sock
    RSHUF=$(shuf -i 30-35 -n 1)
    REDIS_PASS=$(tr -dc "a-zA-Z0-9@#*=" < /dev/urandom | fold -w "$RSHUF" | head -n 1)
  # Extra security
    CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e `uname -r | cut -f1,2 -d"-"` | grep -e [0-9] | xargs sudo apt -y purge)
    SPAMHAUS=/etc/spamhaus.wl
    ENVASIVE=/etc/apache2/mods-available/mod-evasive.load
    APACHE2=/etc/apache2/apache2.conf

# Set Functions
echo "Setting Functions"
  # ask_yes_or_no
    ask_yes_or_no() {
        read -r -p "$1 ([y]es or [N]o): "
        case ${REPLY,,} in
            y|yes)
                echo "yes"
            ;;
            *)
                echo "no"
            ;;
        esac
    }

  # check_command
    check_command() {
      eval "$*"
      if [ ! $? -eq 0 ]; then
         printf "${IRed}Sorry but something went wrong. Please report this issue to $ISSUES and include the output of the error message. Thank you!${Color_Off}\n"
         echo "$* failed"
        exit 1
      fi
    }

  # network_ok
    network_ok() {
        echo "Testing if network is OK..."
        service networking restart
        if wget -q -T 20 -t 2 http://github.com -O /dev/null & spinner_loading
        then
            return 0
        else
            return 1
        fi
    }

  # Whiptail auto-size
    calc_wt_size() {
        WT_HEIGHT=17
        WT_WIDTH=$(tput cols)

        if [ -z "$WT_WIDTH" ] || [ "$WT_WIDTH" -lt 60 ]; then
            WT_WIDTH=80
        fi
        if [ "$WT_WIDTH" -gt 178 ]; then
            WT_WIDTH=120
        fi
        WT_MENU_HEIGHT=$((WT_HEIGHT-7))
        export WT_MENU_HEIGHT
    }

  # download_verify_nextcloud_stable
    download_verify_nextcloud_stable() {
    wget -q -T 10 -t 2 "$NCREPO/$STABLEVERSION.tar.bz2" -P "$HTML"
    mkdir -p "$GPGDIR"
    wget -q "$NCREPO/$STABLEVERSION.tar.bz2.asc" -P "$GPGDIR"
    chmod -R 600 "$GPGDIR"
    gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$OpenPGP_fingerprint"
    gpg --verify "$GPGDIR/$STABLEVERSION.tar.bz2.asc" "$HTML/$STABLEVERSION.tar.bz2"
    rm -r "$GPGDIR"
    }

  # Initial download of script in ../static
    # call like: download_static_script name_of_script
    download_static_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if ! { wget -q "${STATIC}/${1}.sh" -P "$SCRIPTS" || wget -q "${STATIC}/${1}.php" -P "$SCRIPTS" || wget -q "${STATIC}/${1}.py" -P "$SCRIPTS"; }
        then
            echo "{$1} failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|.php|.py' again."
            echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
            echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
            exit 1
        fi
    }

  # Initial download of script in ../lets-encrypt
    # call like: download_le_script name_of_script
    download_le_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if ! { wget -q "${LETS_ENC}/${1}.sh" -P "$SCRIPTS" || wget -q "${LETS_ENC}/${1}.php" -P "$SCRIPTS" || wget -q "${LETS_ENC}/${1}.py" -P "$SCRIPTS"; }
        then
            echo "{$1} failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|.php|.py' again."
            echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
            echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
            exit 1
        fi
    }

  # Run any script in ../static
    # call like: run_static_script name_of_script
    run_static_script() {
        # Get ${1} script
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if wget -q "${STATIC}/${1}.sh" -P "$SCRIPTS"
        then
            bash "${SCRIPTS}/${1}.sh"
            rm -f "${SCRIPTS}/${1}.sh"
        elif wget -q "${STATIC}/${1}.php" -P "$SCRIPTS"
        then
            php "${SCRIPTS}/${1}.php"
            rm -f "${SCRIPTS}/${1}.php"
        elif wget -q "${STATIC}/${1}.py" -P "$SCRIPTS"
        then
            python "${SCRIPTS}/${1}.py"
            rm -f "${SCRIPTS}/${1}.py"
        else
            echo "Downloading ${1} failed"
            echo "Script failed to download. Please run: 'sudo wget ${STATIC}/${1}.sh|php|py' again."
            sleep 3
        fi
    }

  # Run any script in ../apps
    # call like: run_app_script collabora|nextant|passman|spreedme|contacts|calendar|webmin
    run_app_script() {
        rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
        if wget -q "${APP}/${1}.sh" -P "$SCRIPTS"
        then
            bash "${SCRIPTS}/${1}.sh"
            rm -f "${SCRIPTS}/${1}.sh"
        elif wget -q "${APP}/${1}.php" -P "$SCRIPTS"
        then
            php "${SCRIPTS}/${1}.php"
            rm -f "${SCRIPTS}/${1}.php"
        elif wget -q "${APP}/${1}.py" -P "$SCRIPTS"
        then
            python "${SCRIPTS}/${1}.py"
            rm -f "${SCRIPTS}/${1}.py"
        else
            echo "Downloading ${1} failed"
            echo "Script failed to download. Please run: 'sudo wget ${APP}/${1}.sh|php|py' again."
            sleep 3
        fi
    }

  # version
    version(){
        local h t v

        [[ $2 = "$1" || $2 = "$3" ]] && return 0

        v=$(printf '%s\n' "$@" | sort -V)
        h=$(head -n1 <<<"$v")
        t=$(tail -n1 <<<"$v")

        [[ $2 != "$h" && $2 != "$t" ]]
    }

  # version_gt
    version_gt() {
        local v1 v2 IFS=.
        read -ra v1 <<< "$1"
        read -ra v2 <<< "$2"
        printf -v v1 %03d "${v1[@]}"
        printf -v v2 %03d "${v2[@]}"
        [[ $v1 > $v2 ]]
    }

  # spinner_loading
    spinner_loading() {
        pid=$!
        spin='-\|/'
        i=0
        while kill -0 $pid 2>/dev/null
        do
            i=$(( (i+1) %4 ))
            printf "\r[${spin:$i:1}] " # Add text here, something like "Please be paitent..." maybe?
            sleep .1
        done
    }

  # any_key
    any_key() {
        local PROMPT="$1"
        read -r -p "$(printf "${Green}${PROMPT}${Color_Off}")" -n1 -s
        echo
    }

# Set BASH Colors
echo "Setting Colors"
  # Reset
    Color_Off='\e[0m'       # Text Reset

  # Regular Colors
    Black='\e[0;30m'        # Black
    Red='\e[0;31m'          # Red
    Green='\e[0;32m'        # Green
    Yellow='\e[0;33m'       # Yellow
    Blue='\e[0;34m'         # Blue
    Purple='\e[0;35m'       # Purple
    Cyan='\e[0;36m'         # Cyan
    White='\e[0;37m'        # White

  # Bold
    BBlack='\e[1;30m'       # Black
    BRed='\e[1;31m'         # Red
    BGreen='\e[1;32m'       # Green
    BYellow='\e[1;33m'      # Yellow
    BBlue='\e[1;34m'        # Blue
    BPurple='\e[1;35m'      # Purple
    BCyan='\e[1;36m'        # Cyan
    BWhite='\e[1;37m'       # White

  # Underline
    UBlack='\e[4;30m'       # Black
    URed='\e[4;31m'         # Red
    UGreen='\e[4;32m'       # Green
    UYellow='\e[4;33m'      # Yellow
    UBlue='\e[4;34m'        # Blue
    UPurple='\e[4;35m'      # Purple
    UCyan='\e[4;36m'        # Cyan
    UWhite='\e[4;37m'       # White

  # Background
    On_Black='\e[40m'       # Black
    On_Red='\e[41m'         # Red
    On_Green='\e[42m'       # Green
    On_Yellow='\e[43m'      # Yellow
    On_Blue='\e[44m'        # Blue
    On_Purple='\e[45m'      # Purple
    On_Cyan='\e[46m'        # Cyan
    On_White='\e[47m'       # White

  # High Intensity
    IBlack='\e[0;90m'       # Black
    IRed='\e[0;91m'         # Red
    IGreen='\e[0;92m'       # Green
    IYellow='\e[0;93m'      # Yellow
    IBlue='\e[0;94m'        # Blue
    IPurple='\e[0;95m'      # Purple
    ICyan='\e[0;96m'        # Cyan
    IWhite='\e[0;97m'       # White

  # Bold High Intensity
    BIBlack='\e[1;90m'      # Black
    BIRed='\e[1;91m'        # Red
    BIGreen='\e[1;92m'      # Green
    BIYellow='\e[1;93m'     # Yellow
    BIBlue='\e[1;94m'       # Blue
    BIPurple='\e[1;95m'     # Purple
    BICyan='\e[1;96m'       # Cyan
    BIWhite='\e[1;97m'      # White

  # High Intensity backgrounds
    On_IBlack='\e[0;100m'   # Black
    On_IRed='\e[0;101m'     # Red
    On_IGreen='\e[0;102m'   # Green
    On_IYellow='\e[0;103m'  # Yellow
    On_IBlue='\e[0;104m'    # Blue
    On_IPurple='\e[0;105m'  # Purple
    On_ICyan='\e[0;106m'    # Cyan
    On_IWhite='\e[0;107m'   # White

# Pre-Config
echo "Performing Pre-Config"
  # Preinstall Cleanup
    rm /etc/apt/apt.conf.d/50unattended-upgrades.ucf-old
    rm /etc/apt/apt.conf.d/50unattended-upgrades.ucf-dist
  # Prefer IPV4
    sed -i "s|#precedence ::ffff:0:0/96  100|precedence ::ffff:0:0/96  100|g" /etc/gai.conf
  # Make Dirs
    mkdir /var/ncdata
    mkdir /var/scripts
  # Set Mariadb binlog_format to MIXED
    mysql -u root -p$NCPASS -e "SET GLOBAL binlog_format = 'MIXED';"
    sed -i "/skip-external-locking/ a binlog_format = 'MIXED'" /etc/mysql/my.cnf
  # Change InnoDB settings for Mariadb (Moved to MySQL-file-reformat.sh)
    sed -i "/binlog_format = 'MIXED'/ a innodb_large_prefix=true" /etc/mysql/my.cnf
    sed -i "/innodb_large_prefix=true/ a innodb_file_format=barracuda" /etc/mysql/my.cnf
    sed -i "/innodb_file_format=barracuda/ a innodb_file_per_table=1" /etc/mysql/my.cnf
  # Restart MySQL
    /etc/init.d/mysql stop
    /etc/init.d/mysql start

# Install Script Dependencies
echo "Installing Script Dependencies"
apt-get -y update
apt-get -y upgrade
apt-get -y install python-pip
pip install --upgrade pip
cp /etc/apt/sources.list /etc/apt/sources.list.backup
rm -rf /var/cache/apt/archives/*.deb
apt-get -y update
apt-get -y upgrade
#apt-get -y install apache2 apache2-utils
#systemctl start apache2
#systemctl enable apache2
apt-get -y install libclone-perl libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl libipc-sharedcache-perl libwww-perl tinyca libaio1 libcgi-fast-perl libcgi-pm-perl libdbd-mysql-perl libdbi-perl libencode-locale-perl libfcgi-perl libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl liblwp-mediatypes-perl libmysqlclient20 libterm-readkey-perl libtimedate-perl liburi-perl mysql-common bsd-mailx #pdo_mysql
#apt-get -y install software-properties-common
#apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
#add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://www.ftp.saix.net/DB/mariadb/repo/10.1/ubuntu trusty main'
#sudo apt-get update
#mysql-server-5.7 #NCPASS=nextcloud
#apt-get -y install mariadb-common mariadb-server-core-10.0 mariadb-client-core-10.0 mariadb-client-10.0 #mariadb-server-10.0 mariadb-test-data mariadb-test
rm -rf /var/cache/apt/archives/*.deb
apt-get -y install libapache2-mod-php7.0 #libapache2-mod-fcgid
apt-get -y install php7.0-common php7.0-mysql php7.0-intl php7.0-mcrypt php7.0-ldap php7.0-imap php7.0-cli php7.0-gd php7.0-pgsql php7.0-json php7.0-sqlite3 php7.0-curl php7.0-xml php7.0-zip php7.0-mbstring php-smbclient php-imagick php-ftp php-exif php7.0-gmp php-apcu
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb
apt-get -y install unzip
apt-get -y install libav-tools libavdevice-dev libavformat-dev libavfilter-dev libavcodec-dev libswscale-dev libavutil-dev
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb
a2enmod rewrite headers env dir mime ssl setenvif
service apache2 restart
service mysql restart

# Download and validate Nextcloud package
echo "Downloading and validating Nextcloud"
wget -q "${NCREPO}/${STABLEVERSION}.zip" -P ${HTML}
mkdir -p $GPGDIR
wget -q "${NCREPO}/${STABLEVERSION}.zip.asc" -P ${GPGDIR}
chmod -R 600 $GPGDIR
gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$OpenPGP_fingerprint"
gpg --verify $GPGDIR/$STABLEVERSION.zip.asc $HTML/$STABLEVERSION.zip
rm -r $GPGDIR

# Download MySQL-file-reformat.sh
#wget -q $GITLAB/MySQL-file-reformat.sh -P $SCRIPTS
if [ -f $SCRIPTS/MySQL-file-reformat.sh ]
then
    rm $SCRIPTS/MySQL-file-reformat.sh
    wget -q $GITLAB/MySQL-file-reformat.sh -P $SCRIPTS
else
    wget -q $GITLAB/MySQL-file-reformat.sh -P $SCRIPTS
fi

# Extract package
echo "Extracting package"
unzip -q $HTML/$STABLEVERSION.zip -d $HTML
rm $HTML/$STABLEVERSION.zip

# Make $SCRIPTS excutable
echo "Changing permissions"
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

## Secure permissions
wget -q $GITLAB/Nextcloud_Set-File-Permissions.sh -P $SCRIPTS
bash $SCRIPTS/Nextcloud_Set-File-Permissions.sh
chown -R www-data:www-data $NCDATA

# Install Nextcloud
echo "Installing Nextcloud"
cd $NCPATH
sudo -u www-data php occ maintenance:install \
    --data-dir "$NCDATA" \
    --database "mysql" \
    --database-name "nextcloud_db" \
    --database-user "root" \
    --database-pass "$NCPASS" \
    --admin-user "ncadmin" \
    --admin-pass "$NCPASS"
# for some reason the command (--admin-user "ncadmin") actually makes a user named (oc_ncadmin). This must be tested to see if it interferes with anything.
sudo -u www-data php $NCPATH/occ status
cd

# Run MySQL-file-reformat.sh to enable UTF8mb4 (4-byte support)
bash $SCRIPTS/MySQL-file-reformat.sh

# Prepare cron.php to be run every 15 minutes
echo "Preparing cron.php"
crontab -u www-data -l | { cat; echo "*/15  *  *  *  * php -f $NCPATH/cron.php > /dev/null 2>&1"; } | crontab -u www-data -

# Change values in php.ini (increase max file size)
echo "Changing values in php.ini"
# max_execution_time
sed -i "s|max_execution_time = 30|max_execution_time = 3500|g" /etc/php/7.0/apache2/php.ini
# max_input_time
sed -i "s|max_input_time = 60|max_input_time = 3600|g" /etc/php/7.0/apache2/php.ini
# memory_limit
sed -i "s|memory_limit = 128M|memory_limit = 512M|g" /etc/php/7.0/apache2/php.ini
# post_max
sed -i "s|post_max_size = 8M|post_max_size = 1100M|g" /etc/php/7.0/apache2/php.ini
# upload_max
sed -i "s|upload_max_filesize = 2M|upload_max_filesize = 1000M|g" /etc/php/7.0/apache2/php.ini

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
sed -i 's/  php_value upload_max_filesize 511M/# php_value upload_max_filesize 511M/g' $NCPATH/.htaccess
sed -i 's/  php_value post_max_size 511M/# php_value post_max_size 511M/g' $NCPATH/.htaccess
sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess

# Install Figlet
apt install figlet -y

# Generate /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "Generating sites-available files"
touch "/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
echo "<VirtualHost *:80>" > /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    DocumentRoot /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory /var/www/nextcloud>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    AllowOverride All" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all granted" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Satisfy Any" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <IfModule mod_dav.c>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Dav off" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </IfModule>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory "/var/ncdata">" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    # just in case if .htaccess gets disabled" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all denied" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HTTP_HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "</VirtualHost>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf

# Generate /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
touch "/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
echo "<VirtualHost *:443>" > /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Header add Strict-Transport-Security: XXXXXX" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
NEW_STRING='"max-age=15768000;includeSubdomains"'
sed -i "s/XXXXXX/$NEW_STRING/" /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    SSLEngine on" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    DocumentRoot /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    <Directory /var/www/nextcloud>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    AllowOverride All" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Require all granted" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Satisfy Any" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    <IfModule mod_dav.c>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Dav off" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    </IfModule>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    <Directory "/var/ncdata">" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    # just in case if .htaccess gets disabled" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    Require all denied" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    SetEnv HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    SetEnv HTTP_HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "### LOCATION OF CERT FILES ###" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf
echo "</VirtualHost>" >> /etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf

# Enable new config
a2ensite nextcloud_ssl_domain_self_signed.conf
a2ensite nextcloud_http_domain_self_signed.conf
a2dissite default-ssl
service apache2 restart

## Set config values
echo "Setting Nextcloud values"
# Experimental apps
sudo -u www-data php $NCPATH/occ config:system:set appstore.experimental.enabled --value="true"
# Default mail server as an example (make this user configurable?)
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpmode --value="smtp"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpauth --value="1"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpport --value="465"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtphost --value="smtp.gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpauthtype --value="LOGIN"
sudo -u www-data php $NCPATH/occ config:system:set mail_from_address --value="www.example.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_domain --value="gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpsecure --value="ssl"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpname --value="www.example.com@gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtppassword --value="vinr vhpa jvbh hovy"

# Install Libreoffice Writer to be able to read MS documents.
echo "Installing Libreoffice Writer"
sudo apt install --no-install-recommends libreoffice-writer -y
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb

# Nextcloud apps
echo "Downloading Nextcloud apps"
CONVER=$(curl -s https://api.github.com/repos/nextcloud/contacts/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
CONVER_FILE=contacts.tar.gz
CONVER_REPO=https://github.com/nextcloud/contacts/releases/download
CALVER=$(curl -s https://api.github.com/repos/nextcloud/calendar/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
CALVER_FILE=calendar.tar.gz
CALVER_REPO=https://github.com/nextcloud/calendar/releases/download
sudo -u www-data php $NCPATH/occ config:system:set preview_libreoffice_path --value="/usr/bin/libreoffice"

# Download and install Calendar
wget -q $CALVER_REPO/v$CALVER/$CALVER_FILE -P $NCPATH/apps
tar -zxf $NCPATH/apps/$CALVER_FILE -C $NCPATH/apps
cd $NCPATH/apps
rm $CALVER_FILE

# Enable Calendar
sudo -u www-data php $NCPATH/occ app:enable calendar

# Download and install Contacts
wget -q $CONVER_REPO/v$CONVER/$CONVER_FILE -P $NCPATH/apps
tar -zxf $NCPATH/apps/$CONVER_FILE -C $NCPATH/apps
cd $NCPATH/apps
rm $CONVER_FILE

# Enable Contacts
sudo -u www-data php $NCPATH/occ app:enable contacts

# Install packages for Webmin
apt install -y zip perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python

# Install Webmin
sed -i '$a deb http://download.webmin.com/download/repository sarge contrib' /etc/apt/sources.list
wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
apt update -q2
apt install webmin -y

# Confirm Nextcloud apps installs
whiptail --title "Which apps/programs do you want to install?" --checklist --separate-output "" 10 40 3 \
"Calendar" "              " on \
"Contacts" "              " on \
"Webmin" "              " on 2>results

### Skipped ###
# Change roots .bash_profile
#wget -q $STATIC/change-root-profile.sh -P $SCRIPTS

### Skipped ###
# Change $UNIXUSER .bash_profile
#wget -q $STATIC/change-ncadmin-profile.sh -P $SCRIPTS

### Changed ###
# Welcome message after login (change in $HOME/.profile
wget -q $GITLAB/instruction.sh -P $SCRIPTS

### Changed ###
# Get nextcloud-first-run-startup.sh
wget -q $GITLAB/nextcloud-first-run-startup.sh -P $SCRIPTS

# Get nextcloud-every-startup.sh
wget -q $GITLAB/nextcloud-every-startup.sh -P $SCRIPTS

# Get update.sh to automate updates
wget -q $STATIC/update.sh -P $SCRIPTS

# Get script to clear history on every login
wget -q $STATIC/history.sh -P $SCRIPTS

# Get script for Redis
wget -q $STATIC/redis-server-ubuntu16.sh -P $SCRIPTS

# Install Redis
bash $SCRIPTS/redis-server-ubuntu16.sh

# Upgrade
apt update -q2
apt full-upgrade -y

# Remove LXD (always shows up as failed during boot)
apt purge lxd -y

# Cleanup
apt autoremove -y
apt autoclean
rm -rf /var/cache/apt/archives/*.deb

# Set permissions for last time (./data/.htaccess has wrong permissions otherwise)
bash $SCRIPTS/Nextcloud_Set-File-Permissions.sh

# Shutdown, next boot is for Clent setup
echo "Installation done, system will now shutdown so a template can be made..."
shutdown -h now

exit 0

#---------------
# The following optional packages for Nextcloud 11 have not been installed:
#PHP module memcached
#PHP module pcntl
