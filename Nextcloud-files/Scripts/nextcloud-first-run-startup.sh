#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

printf "%s\n""\e[0;32m" && echo  "Starting automatic configuration of Nextcloud, please be patient and wait for the next prompt." && printf "\e[0m\n"

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud-files/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

  # download_app_script name_of_script
  download_app_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${APP}/${1}.sh" -P "$SCRIPTS" || wget -q "${APP}/${1}.php" -P "$SCRIPTS" || wget -q "${APP}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${APP}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

  # download_gitlab_script name_of_script
  download_gitlab_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

printf "%s\n""\e[0;32m" && echo "Updateing scripts from the web to be able to run the first setup..." && printf "\e[0m\n"
# Most of the shell scripts in static (.sh)
download_static_script security
download_static_script update
download_static_script trusted
download_static_script setup_secure_permissions_nextcloud
download_static_script change_mysql_pass
download_static_script nextcloud
download_static_script update-config
download_static_script index
download_static_script history

# Most of the shell scripts in app (.sh)
download_app_script collabora
download_app_script nextant
download_app_script passman
download_app_script spreedme

# Most of the shell scripts in gitlab (.sh)
download_gitlab_script instruction
download_gitlab_script timezone
download_gitlab_script change-NCADMIN-password
download_gitlab_script fail2ban
download_gitlab_script sync-all-scripts
download_gitlab_script nextcloud-first-run-startup
download_gitlab_script nextcloud-every-startup

# Get temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    printf "\e[0mScript failed to download. Please run: \e[0;36m\'sudo bash $SCRIPTS/nextcloud-startup-script.sh'\e[0m again\n"
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

printf "%s\n""\e[0;32m" && echo "Securing permissions" && printf "\e[0m\n"
# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

clear
echo "+--------------------------------------------------------------------+"
echo "| This script will configure your Nextcloud and activate SSL.        |"
echo "| It will also do the following:                                     |"
echo "|                                                                    |"
echo "| - Set new keyboard layout                                          |"
echo "| - Change timezone                                                  |"
#echo "| - Generate new SSH keys for the server                             |"
echo "| - Generate new MySQL password                                      |"
echo "| - Configure UTF8mb4 (4-byte support for MySQL)                     |"
echo "| - Install phpMyadmin and make it secure                            |"
echo "| - Install selected apps and automatically configure them           |"
#echo "| - Detect and set hostname                                          |"
echo "| - Allow you to automate Nextcloud updates                          |"
echo "| - Allow you to automatically install fail2ban                      |"
echo "| - Upgrade your system and Nextcloud to latest version              |"
echo "| - Set secure permissions to Nextcloud                              |"
#echo "| - Set new passwords to Linux and Nextcloud                         |"
#echo "| - Set static IP to the system (you have to set the same IP in      |"
#echo "|   your router) https://www.techandme.se/open-port-80-443/          |"
#echo "|   We don't set static IP if you run this on a *remote* VPS.        |"
echo "|                                                                    |"
echo "|   The script will take about 10 minutes to finish,                 |"
echo "|   depending on your internet connection.                           |"
echo "|                                                                    |"
echo "+--------------------------------------------------------------------+"
any_key "Press any key to start the script..."
clear


# Set keyboard layout
echo "Current keyboard layout is $(localectl status | grep "Layout" | awk '{print $3}')"
if [[ "no" == $(ask_yes_or_no "Do you want to change keyboard layout?") ]]
then
    printf "%s\n""\e[0;32m" && echo "Not changing keyboard layout..." && printf "\e[0m\n"
    any_key "Press any key to continue..."
    sleep 1
    clear
else
    dpkg-reconfigure keyboard-configuration
clear
fi

# Change Timezone
if [[ "yes" == $(ask_yes_or_no "Do you want to change your timezone?") ]]
then
	dpkg-reconfigure tzdata
	sleep 3
  any_key "Press any key to continue..."
else
    echo
    printf "%s\n""\e[0;32m" && echo "OK, Current timezone is $(cat /etc/timezone)" && printf "\e[0m\n"
    printf "\e[0mHowever, if you want to change it later, just type: \e[0;36m\'sudo bash $SCRIPTS/timezone.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

# Pretty URLs
printf "%s\n""\e[0;32m" && echo "Setting RewriteBase to \"/\" in config.php..." && printf "\e[0m\n"
chown -R www-data:www-data $NCPATH
sudo -u www-data php $NCPATH/occ config:system:set htaccess.RewriteBase --value="/"
sudo -u www-data php $NCPATH/occ maintenance:update:htaccess
bash $SECURE & spinner_loading

# Generate new SSH Keys
#printf "\nGenerating new SSH keys for the server...\n"
#rm -v /etc/ssh/ssh_host_*
#dpkg-reconfigure openssh-server

# Generate new MySQL password
printf "%s\n""\e[0;32m" && echo "Generating new MySQL password..." && printf "\e[0m\n"
if bash "$SCRIPTS/change_mysql_pass.sh" && wait
then
   rm "$SCRIPTS/change_mysql_pass.sh"
   {
   echo "[mysqld]"
   echo "innodb_large_prefix=on"
   echo "innodb_file_format=barracuda"
   echo "innodb_file_per_table=1"
   } >> /root/.my.cnf
fi
printf "%s\n""\e[0;36m" && echo "Please make a record of the new MySQL root password above" && printf "\e[0m\n"
any_key "Press any key to continue..."

# Enable UTF8mb4 (4-byte support)
NCDB=nextcloud_db
PW_FILE=/var/mysql_password.txt
printf "\nEnabling UTF8mb4 support on $NCDB....\n"
printf "%s\n""\e[0;32m" && echo "Enabling UTF8mb4 (4-byte support). Please be patient, it may take a while." && printf "\e[0m\n"
sudo /etc/init.d/mysql restart & spinner_loading
RESULT="mysqlshow --user=root --password=$(cat $PW_FILE) $NCDB| grep -v Wildcard | grep -o $NCDB"
if [ "$RESULT" == "$NCDB" ]; then
    check_command mysql -u root -e "ALTER DATABASE $NCDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"
    wait
fi
check_command sudo -u www-data $NCPATH/occ config:system:set mysql.utf8mb4 --type boolean --value="true"
check_command sudo -u www-data $NCPATH/occ maintenance:repair

# Install phpMyadmin
run_app_script phpmyadmin_install_ubuntu16
clear

cat << LETSENC
+-----------------------------------------------+
|  The following script will install a trusted  |
|  SSL certificate through Lets Encrypt.        |
+-----------------------------------------------+
LETSENC

# Lets Encrypt
printf "\e[32mBefore we begin the installation you need to have a domain that the SSL certs will be valid for. If you don't have a domain yet, get one before you run this script! \e[0m\n"
if [[ "yes" == $(ask_yes_or_no "Do you want to install SSL?") ]]
then
    bash $SCRIPTS/activate-ssl.sh
else
    echo
    printf "\e[0mOK, but please note that some apps like \e[0;36mCollabora\e[0m & \e[0;36mNextant\e[0m will not work without ssl being enabled\n"
    printf "\e[0mIf you want to run this script later, just type: \e[0;36m'sudo bash $SCRIPTS/activate-ssl.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "These apps will only work if you installed SSL via the Lets Encrypt script you were asked to install above. If you did not install SSL, then DO NOT try to install these apps as the install will fail" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Collabora" "(Online editing)   " OFF \
"Nextant" "(Full text search)   " OFF 2>results

while read -r -u 9 choice
do
    case $choice in
        Collabora)
            run_app_script collabora
        ;;

        Nextant)
            run_app_script nextant
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

printf "\e[0mIf you want to install collabora later, just type: \e[0;36m'sudo bash $SCRIPTS/collabora.sh'\e[0m\n"
printf "\e[0mIf you want to install nextant later, just type: \e[0;36m'sudo bash $SCRIPTS/nextant.sh'\e[0m\n"
any_key "Press any key to continue..."
clear

whiptail --title "Which apps do you want to install?" --checklist --separate-output "Automatically configure and install selected apps\nSelect by pressing the spacebar" "$WT_HEIGHT" "$WT_WIDTH" 4 \
"Passman" "(Password storage)   " OFF \
"Spreed.ME" "(Video calls)   " OFF 2>results

while read -r -u 9 choice
do
    case $choice in
        Passman)
            run_app_script passman
        ;;

        Spreed.ME)
            run_app_script spreedme
        ;;

        *)
        ;;
    esac
done 9< results
rm -f results
clear

printf "\e[0mIf you want to install passman later, just type: \e[0;36m'sudo bash $SCRIPTS/passman.sh'\e[0m\n"
printf "\e[0mIf you want to install spreedme later, just type: \e[0;36m'sudo bash $SCRIPTS/spreedme.sh'\e[0m\n"
any_key "Press any key to continue..."
clear

# Ask to automate Nextcloud updates
if [[ "yes" == $(ask_yes_or_no "Do you want to automate Nextcloud updates ?") ]]
then
    crontab -u root -l | { cat; echo "0 4 * * 1 bash /var/scripts/sync-all-scripts.sh"; } | crontab -u root -
    crontab -u root -l | { cat; echo "0 5 * * 1 bash /var/scripts/update.sh"; } | crontab -u root -
    printf "\e[32msync-all-scripts.sh \e[0;0mwill run every \e[36mMonday at 04:00 (24 hr)\e[0m \n"
    printf "\e[32mupdate.sh \e[0;0mwill run every \e[36mMonday at 05:00 (24 hr)\e[0m \n"
    printf "\e[0mYou may edit this at anytime by typing: \e[0;36m\'crontab -u root -e'\e[0m\n"
    any_key "Press any key to continue..."
else
    echo
    printf "\e[0mOK, To update Nextcloud manually, type: \e[0;36m\'sudo bash $SCRIPTS/update.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

# Ask to install fail2ban
if [[ "yes" == $(ask_yes_or_no "Do you want to install fail2ban and have a strict sshd jail setup ?") ]]
then
    bash $SCRIPTS/fail2ban.sh
    echo
    printf "\e[32mfail2ban \e[0;0mhas been installed and a \e[36mstrict sshd jail\e[0m has been setup.\n"
    printf "\e[0mYou may edit this at anytime by typing: \e[0;36m\'nano /etc/fail2ban/jail.local'\e[0m\n"
    printf "\e[0mIf you want to learn more about fail2ban and how to use it type: \e[0;36m\'cat /etc/fail2ban/info.txt'\e[0m\n"
    any_key "Press any key to continue..."

else
    echo
    printf "\e[0mOK, but if you want to run it later, just type: \e[0;36m\'sudo bash $SCRIPTS/fail2ban.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

# Add extra security
if [[ "yes" == $(ask_yes_or_no "Do you want to add extra security, based on this: http://goo.gl/gEJHi7 ?") ]]
then
    bash $SCRIPTS/security.sh
    rm "$SCRIPTS"/security.sh
else
    echo
    printf "\e[0mOK, but if you want to run it later, just type: \e[0;36m\'sudo bash $SCRIPTS/security.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

## Change UNIXUSER password (Skipped)
#printf "${Color_Off}\n"
#echo "For better security, change the system user password for [$UNIXUSER]"
#any_key "Press any key to change password for system user..."
#while true
#do
#    sudo passwd "$UNIXUSER" && break
#done
#echo
#clear

## Change NCADMIN password
NCADMIN=$(sudo -u www-data php $NCPATH/occ user:list | awk '{print $3}')
printf "${Color_Off}\n"
printf "\e[0mFor better security, change the Default Nextcloud password for \e[0;36m$NCADMIN\e[0m\n"
printf "\e[0mThe current password for \e[0;32m$NCADMIN\e[0m is \e[0;36m$NCPASS\e[0m\n"
if [[ "yes" == $(ask_yes_or_no "Do you want to change the Default Nextcloud password for $NCADMIN ?") ]]
then
    sudo -u www-data php "$NCPATH/occ" user:resetpassword "$NCADMIN" && break
else
    echo
    echo "OK, but your Nextcloud server will not be propperly secured until this password is updated."
    printf "%s\n""\e[0;32m" && echo "OK, but your Nextcloud server will not be propperly secured until this password is updated." && printf "\e[0m\n"
    printf "\e[0mTo update this password from the CLI, just type: \e[0;36m'sudo bash $SCRIPTS/change-NCADMIN-password.sh'\e[0m\n"
    any_key "Press any key to continue..."
fi
clear

# Fixes https://github.com/nextcloud/vm/issues/58
a2dismod status
service apache2 reload

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
# Here is a guide: https://www.techandme.se/increase-max-file-size/
VALUE="# php_value upload_max_filesize 513M"
if ! grep -Fxq "$VALUE" $NCPATH/.htaccess
then
        sed -i 's/  php_value upload_max_filesize 513M/# php_value upload_max_filesize 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value post_max_size 513M/# php_value post_max_size 513M/g' $NCPATH/.htaccess
        sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess
fi

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Prepare every bootup
#check_command run_static_script change-ncadmin-profile
check_command run_gitlab_script change-root-profile_every-run

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
rm -f "$SCRIPTS/ip.sh"
rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

# Upgrade system
printf "%s\n""\e[0;32m" && echo "System will now upgrade..." && printf "\e[0m\n"
bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"
# Delete MySQL password from /root/.my.cnf (for security resons)
sed -i '/password/d' /root/.my.cnf

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!
clear

#ADDRESS3=$(hostname -I | cut -d ' ' -f 1)
#printf "%s\n""\e[0;32m"
#echo    "+-----------------------------------------------------------------------+"
#printf "|    \e[0mThanks for using this Nextcloud server!\e[0;32m                            |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mLogin to Nextcloud with http: \e[0;36m\"http://$ADDRESS3/login\"\e[0;32m        |\n"
#printf "|    \e[0mLogin to Nextcloud with https: \e[0;36m\"https://$ADDRESS3/login\"\e[0;32m      |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mIt is recommended to change the default user.\e[0;32m                      |\n"
#printf "|    \e[0mDo this by adding another admin user, log out from ncadmin,\e[0;32m        |\n"
#printf "|    \e[0mand login with your new user, then delete ncadmin.\e[0;32m                 |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mTo run the first boot script again type:\e[0;32m                           |\n"
#printf "|    \e[0;36m     'sudo bash /var/scripts/nextcloud-first-run-startup.sh'\e[0;32m       |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mEvery root login, the update script will run automatically\e[0;32m         |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mTo update Nextcloud manually, type:\e[0;32m                                |\n"
#printf "|    \e[0;36m     'sudo bash /var/scripts/update.sh'\e[0;32m                            |\n"
#echo    "|                                                                       |"
#printf "|    \e[0mTo login to MySQL just type: \e[0;36m'mysql -u root -p'\e[0;32m                    |\n"
#printf "|    \e[0mand enter the password for mysql which you were asked to record.\e[0;32m   |\n"
#echo    "|                                                                       |"
#echo    "+-----------------------------------------------------------------------+"
#printf "\e[0m\n"

# Set trusted domain in config.php
bash "$SCRIPTS"/trusted.sh
rm -f "$SCRIPTS"/trusted.sh

# Prefer IPv6
sed -i "s|precedence ::ffff:0:0/96  100|#precedence ::ffff:0:0/96  100|g" /etc/gai.conf

# Reboot
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
#echo  "The RSA keys for this server have been changed,"
#echo  "you must either add the correct host key in '~/.ssh/known_hosts'"
#echo  "or delete '~/.ssh/known_hosts' inorder to be able to ssh again."
any_key "Installation finished, press any key to reboot system..."
reboot
