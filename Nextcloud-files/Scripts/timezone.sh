#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

printf "%s\n""\e[0;32m" && echo  "Starting script, please be patient." && printf "\e[0m\n"

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud-files/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

  # download_gitlab_script name_of_script
  download_gitlab_script() {
      # Get ${1} script
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if ! { wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS" || wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"; }
      then
          echo "{$1} failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|.php|.py' again."
          echo "If you get this error when running the nextcloud-startup-script then just re-run it with:"
          echo "'sudo bash $SCRIPTS/nextcloud-startup-script.sh' and all the scripts will be downloaded again"
          exit 1
      fi
  }

# Change Timezone
if [[ "yes" == $(ask_yes_or_no "Do you want to change your timezone?") ]]
then
	dpkg-reconfigure tzdata
	sleep 3
  any_key "Press any key to continue..."
else
    echo
    echo "OK, Current timezone is $(cat /etc/timezone)"
    echo "However, if you want to change it later, just type: sudo bash $SCRIPTS/timezone.sh"
    any_key "Press any key to continue..."
fi
