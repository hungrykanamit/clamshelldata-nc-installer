#!/bin/bash

# Tech and Me © - 2017, https://www.techandme.se/
# Tweaks to Original scripts by Clamshell Data, https://clamshelldata.com

# shellcheck disable=2034,2059
true
# shellcheck source=lib.sh
FIRST_IFACE=1 && CHECK_CURRENT_REPO=1 . <(curl -sL https://raw.githubusercontent.com/nextcloud/vm/master/lib.sh)
unset FIRST_IFACE
unset CHECK_CURRENT_REPO

## Check where the best mirrors are and update (Skipped, not essential and buggy)
#printf "\nTo make downloads as fast as possible when updating you should have mirrors that are as close to you as possible.\n"
#echo "This VM comes with mirrors based on servers in that where used when the VM was released and packaged."
#echo "We recomend you to change the mirrors based on where this is currently installed."
#echo "Checking current mirror..."
#printf "Your current server repository is:  ${Cyan}$REPO${Color_Off}\n"
#
#if [[ "no" == $(ask_yes_or_no "Do you want to try to find a better mirror?") ]]
#then
#    echo "Keeping $REPO as mirror..."
#    sleep 1
#else
#    echo "Locating the best mirrors..."
#    apt update -q4 & spinner_loading
#    apt install python-pip -y
#    pip install \
#        --upgrade pip \
#        apt-select
#    apt-select -m up-to-date -t 5 -c
#    sudo cp /etc/apt/sources.list /etc/apt/sources.list.backup && \
#    if [ -f sources.list ]
#    then
#        sudo mv sources.list /etc/apt/
#    fi
#fi

# Custom Variables
GITLAB="https://gitlab.com/hungrykanamit/Nextcloud/raw/master/Nextcloud/Scripts/"

# Custom Functions
  # run_gitlab_script name_of_script
  run_gitlab_script() {
      # Get ${1} gitlab
      rm -f "${SCRIPTS}/${1}.sh" "${SCRIPTS}/${1}.php" "${SCRIPTS}/${1}.py"
      if wget -q "${GITLAB}/${1}.sh" -P "$SCRIPTS"
      then
          bash "${SCRIPTS}/${1}.sh"
          rm -f "${SCRIPTS}/${1}.sh"
      elif wget -q "${GITLAB}/${1}.php" -P "$SCRIPTS"
      then
          php "${SCRIPTS}/${1}.php"
          rm -f "${SCRIPTS}/${1}.php"
      elif wget -q "${GITLAB}/${1}.py" -P "$SCRIPTS"
      then
          python "${SCRIPTS}/${1}.py"
          rm -f "${SCRIPTS}/${1}.py"
      else
          echo "Downloading ${1} failed"
          echo "Script failed to download. Please run: 'sudo wget ${GITLAB}/${1}.sh|php|py' again."
          sleep 3
      fi
  }

echo
echo  "Updateing scripts from the web to be able to run the first setup..."
# All the shell scripts in static (.sh)
#download_static_script temporary-fix
download_static_script security
download_static_script update
#download_static_script trusted
#download_static_script ip
#download_static_script test_connection
download_static_script setup_secure_permissions_nextcloud
#download_static_script change_mysql_pass
download_static_script nextcloud
download_static_script update-config
download_static_script index

# Get temporary-fix.sh
if [ -f ${SCRIPTS}/temporary-fix.sh ]
then
    rm ${SCRIPTS}/temporary-fix.sh
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
else
    wget -q "${STATIC}/temporary-fix.sh" -P ${SCRIPTS}
fi

# Get instruction.sh
if [ -f ${SCRIPTS}/instruction.sh ]
then
    rm ${SCRIPTS}/instruction.sh
    wget -q "${GITLAB}/instruction.sh" -P ${SCRIPTS}
else
    wget -q "${GITLAB}/instruction.sh" -P ${SCRIPTS}
fi

# Lets Encrypt
if [ -f "$SCRIPTS"/activate-ssl.sh ]
then
    rm "$SCRIPTS"/activate-ssl.sh
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
else
    wget -q $LETS_ENC/activate-ssl.sh -P "$SCRIPTS"
fi
if [ ! -f "$SCRIPTS"/activate-ssl.sh ]
then
    echo "activate-ssl failed"
    echo "Script failed to download. Please run: 'sudo bash $SCRIPTS/nextcloud-startup-script.sh' again."
    exit 1
fi

mv $SCRIPTS/index.php $HTML/index.php && rm -f $HTML/html/index.html
chmod 750 $HTML/index.php && chown www-data:www-data $HTML/index.php

# Change 000-default to $WEB_ROOT
sed -i "s|DocumentRoot /var/www/html|DocumentRoot $HTML|g" /etc/apache2/sites-available/000-default.conf

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Allow $UNIXUSER to run figlet script
chown "$UNIXUSER":"$UNIXUSER" "$SCRIPTS/nextcloud.sh"

# Add temporary fix if needed
bash $SCRIPTS/temporary-fix.sh
rm "$SCRIPTS"/temporary-fix.sh

# Prepare every bootup
#check_command run_static_script change-ncadmin-profile
check_command run_gitlab_script change-root-profile_every-run

# Cleanup 1
sudo -u www-data php "$NCPATH/occ" maintenance:repair
rm -f "$SCRIPTS/ip.sh"
rm -f "$SCRIPTS/test_connection.sh"
#rm -f "$SCRIPTS/instruction.sh"
rm -f "$NCDATA/nextcloud.log"
#rm -f "$SCRIPTS/nextcloud-startup-script.sh"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete
sed -i "s|instruction.sh|nextcloud.sh|g" "/home/$UNIXUSER/.bash_profile"

truncate -s 0 \
    /root/.bash_history \
    "/home/$UNIXUSER/.bash_history" \
    /var/spool/mail/root \
    "/var/spool/mail/$UNIXUSER" \
    /var/log/apache2/access.log \
    /var/log/apache2/error.log \
    /var/log/cronjobs_success.log

sed -i "s|sudo -i||g" "/home/$UNIXUSER/.bash_profile"
cat << RCLOCAL > "/etc/rc.local"
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

exit 0

RCLOCAL
clear

# Upgrade system
echo "System will now upgrade..."
bash $SCRIPTS/update.sh

# Cleanup 2
apt autoremove -y
apt autoclean -y
rm -rf /var/cache/apt/archives/*.deb
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e "$(uname -r | cut -f1,2 -d"-")" | grep -e "[0-9]" | xargs sudo apt -y purge)
echo "$CLEARBOOT"
# Delete MySQL password from /root/.my.cnf (for security resons)
sed -i '/password/d' /root/.my.cnf

ADDRESS2=$(grep "address" /etc/network/interfaces | awk '$1 == "address" { print $2 }')
# Success!

# Run instruction.sh
bash $SCRIPTS/instruction.sh
